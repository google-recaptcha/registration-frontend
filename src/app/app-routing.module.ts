import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {path: 'recaptcha-v2', loadChildren: () => import ('./layouts/recaptcha-v2/recaptcha-v2.module').then(m => m.RecaptchaV2Module)},
  {path: '', loadChildren: () => import ('./layouts/recaptcha-v2/recaptcha-v2.module').then(m => m.RecaptchaV2Module)},
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
