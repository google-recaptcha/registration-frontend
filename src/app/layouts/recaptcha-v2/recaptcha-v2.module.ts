import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RecaptchaV2RoutingModule } from './recaptcha-v2-routing.module';
import { FormLoginNpmComponent } from './implement-npm/form-login-npm/form-login-npm.component';
import { FormLoginNativeComponent } from './implement-native/form-login-native/form-login-native.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RecaptchaFormsModule, RecaptchaModule} from 'ng-recaptcha';
import {RecaptchaV2Component} from './recaptcha-v2.component';
import { FormLoginInvisibleComponent } from './implement-invisible/form-login-invisible/form-login-invisible.component';


@NgModule({
  declarations: [
    RecaptchaV2Component,
    FormLoginNpmComponent,
    FormLoginNativeComponent,
    FormLoginInvisibleComponent],
  imports: [
    CommonModule,
    RecaptchaV2RoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RecaptchaModule,
    RecaptchaFormsModule
  ]
})
export class RecaptchaV2Module { }
