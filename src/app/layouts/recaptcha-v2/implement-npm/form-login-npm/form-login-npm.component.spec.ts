import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormLoginNpmComponent } from './form-login-npm.component';

describe('FormLoginNpmComponent', () => {
  let component: FormLoginNpmComponent;
  let fixture: ComponentFixture<FormLoginNpmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormLoginNpmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormLoginNpmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
