import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Login} from '../../../../models/login.model';
import {RecaptchaV2Service} from '../../recaptcha-v2.service';

@Component({
  selector: 'app-form-login-npm',
  templateUrl: './form-login-npm.component.html',
  styleUrls: ['./form-login-npm.component.scss']
})
export class FormLoginNpmComponent implements OnInit {
  @ViewChild('email', {static: false}) email: ElementRef;
  EMAIL_VALID = false;
  EMAIL_SUBMITTED = false;
  formGroup: FormGroup;
  login: Login =  new Login();
  token: string;
  constructor(
    private recaptchaV2Service: RecaptchaV2Service
  ) { }

  ngOnInit() {
    this.createForm();
  }
  submitForm() {
    this.EMAIL_SUBMITTED = true;
    if (this.validateFormEmail()) {
      this.formEmailLoginMapper();
      this.recaptchaV2Service.validateEmail(this.login).subscribe(
        (response) => {
            if (response.status === 'success') {
              this.EMAIL_VALID = true;
              alert('VALID');
            } else {
              alert(response.status);
            }
          },
        (err) => { console.log('Error'); },
        () => {
        }
        );
    }
  }
  createForm() {
/*    this.formGroup = new FormGroup({
      name: new FormControl(null, []),
      FirstLastName: new FormControl(null, []),
      SecondLastName: new FormControl(null, []),
      email: new FormControl(null, [Validators.required]),
      user: new FormControl(null, []),
      password: new FormControl(null, [])
    });*/
    this.formGroup = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [])
    });
  }
  validateFormEmail(): boolean {
    const f = this.formGroup.controls;
    if (f.email.invalid) {
      this.email.nativeElement.focus();
      return false;
    }
    if (this.token === null) {
      alert('verify that it is not a robot');
      return false;
    }
    return true;
  }
  formEmailLoginMapper() {
    const f = this.formGroup.controls;
    if (f.email.valid) {
      this.login.email = f.email.value;
      this.login.token = this.token;
    }
  }
  resolved(event) {
    this.token = event;
  }
  get f() {
    return this.formGroup.controls;
  }

}
