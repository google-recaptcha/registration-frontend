import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RecaptchaV2Component} from './recaptcha-v2.component';
import {FormLoginNpmComponent} from './implement-npm/form-login-npm/form-login-npm.component';
import {FormLoginNativeComponent} from './implement-native/form-login-native/form-login-native.component';
import {FormLoginInvisibleComponent} from './implement-invisible/form-login-invisible/form-login-invisible.component';

const routes: Routes = [
  {
    path: '',
    component: RecaptchaV2Component,
    children: [
      { path: 'login-npm', component: FormLoginNpmComponent },
      { path: 'login-native', component: FormLoginNativeComponent },
      { path: 'login-invisible', component: FormLoginInvisibleComponent },
      { path: '**', redirectTo: 'login-npm'},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecaptchaV2RoutingModule { }
