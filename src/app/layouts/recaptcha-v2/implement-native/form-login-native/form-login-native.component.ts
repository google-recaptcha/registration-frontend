import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Login} from '../../../../models/login.model';
import {RecaptchaV2Service} from '../../recaptcha-v2.service';

@Component({
  selector: 'app-form-login-native',
  templateUrl: './form-login-native.component.html',
  styleUrls: ['./form-login-native.component.scss']
})
export class FormLoginNativeComponent implements OnInit {
  @ViewChild('recaptcha', {static: true }) recaptchaElement: ElementRef;
  @ViewChild('email', {static: false}) email: ElementRef;
  private SECRET = '6LfTJrkUAAAAAA7p2HuDFGiGykXPfXF_WZBQRSEX';
  SUBMITTED = false;
  EMAIL_VALID = false;
  EMAIL_SUBMITTED = false;
  formGroup: FormGroup;
  login: Login =  new Login();
  token: string;
  constructor(
    private recaptchaV2Service: RecaptchaV2Service
  ) { }
  ngOnInit() {
    this.createForm();
    this.addRecaptchaScript();
  }
  renderReCaptch() {
    console.log(window);
    window[`grecaptcha`].render(this.recaptchaElement.nativeElement, {
      sitekey : this.SECRET,
      callback : (response) => {
        this.token = response;
      }
    });
  }

  addRecaptchaScript() {
    window[`grecaptchaCallback`] = () => {
      this.renderReCaptch();
    }
    ((d, s, id, obj) => {
      let js;
      const fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {
        obj.renderReCaptch();
        return;
      }
      js = d.createElement(s);
      js.id = id;
      js.src = 'https://www.google.com/recaptcha/api.js?onload=grecaptchaCallback&amp;render=explicit';
      fjs.parentNode.insertBefore(js, fjs);
    })(document, 'script', 'recaptcha-jssdk', this);

  }
  submitForm() {
    this.EMAIL_SUBMITTED = true;
    if (this.validateFormEmail()) {
      this.formEmailLoginMapper();
      this.recaptchaV2Service.validateEmail(this.login).subscribe(
        (response) => {
          if (response.status === 'success') {
            this.EMAIL_VALID = true;
            alert('VALID');
          } else {
            alert(response.status);
          }
        },
        (err) => { console.log('Error'); },
        () => {
        }
      );
    }
  }
  createForm() {
    this.formGroup = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [])
    });
  }
  validateFormEmail(): boolean {
    const f = this.formGroup.controls;
    if (f.email.invalid) {
      this.email.nativeElement.focus();
      return false;
    }
    if (this.token === null) {
      alert('verify that it is not a robot');
      return false;
    }
    return true;
  }
  formEmailLoginMapper() {
    const f = this.formGroup.controls;
    if (f.email.valid) {
      this.login.email = f.email.value;
      this.login.token = this.token;
    }
  }
  get f() {
    return this.formGroup.controls;
  }

}
