import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormLoginNativeComponent } from './form-login-native.component';

describe('FormLoginNativeComponent', () => {
  let component: FormLoginNativeComponent;
  let fixture: ComponentFixture<FormLoginNativeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormLoginNativeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormLoginNativeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
