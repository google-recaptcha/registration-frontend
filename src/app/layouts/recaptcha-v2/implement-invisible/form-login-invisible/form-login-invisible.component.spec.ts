import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormLoginInvisibleComponent } from './form-login-invisible.component';

describe('FormLoginInvisibleComponent', () => {
  let component: FormLoginInvisibleComponent;
  let fixture: ComponentFixture<FormLoginInvisibleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormLoginInvisibleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormLoginInvisibleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
