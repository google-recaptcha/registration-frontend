import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Login} from '../../../../models/login.model';
import {RecaptchaV2Service} from '../../recaptcha-v2.service';

@Component({
  selector: 'app-form-login-invisible',
  templateUrl: './form-login-invisible.component.html',
  styleUrls: ['./form-login-invisible.component.scss']
})
export class FormLoginInvisibleComponent implements OnInit {
  @ViewChild('email', {static: false}) email: ElementRef;
  @ViewChild('captchaRef', {static: false}) captchaRef: any;
  EMAIL_VALID = false;
  EMAIL_SUBMITTED = false;
  formGroup: FormGroup;
  login: Login =  new Login();
  token: string;
  constructor(
    private recaptchaV2Service: RecaptchaV2Service
  ) { }

  ngOnInit() {
    this.createForm();
  }
  submitForm() {
    this.EMAIL_SUBMITTED = true;
    if (this.validateFormEmail()) {
      this.formEmailLoginMapper();
      this.captchaRef.execute();
    }
  }
  createForm() {
    this.formGroup = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [])
    });
  }
  validateFormEmail(): boolean {
    const f = this.formGroup.controls;
    if (f.email.invalid) {
      this.email.nativeElement.focus();
      return false;
    }
    return true;
  }
  resolved(captchaResponse: string) {
    this.login.token = captchaResponse;
    this.recaptchaV2Service.validateEmailInvisibleRecaptcha(this.login).subscribe(
        (response) => {
          if (response.status === 'success') {
            this.EMAIL_VALID = true;
            alert('VALID');
          } else {
            alert(response.status);
          }
          this.captchaRef.reset();
        },
        (err) => {
          console.log('Error');
          this.captchaRef.reset();
        },
        () => {
          this.captchaRef.reset();
        }
      );
  }
  formEmailLoginMapper() {
    const f = this.formGroup.controls;
    if (f.email.valid) {
      this.login.email = f.email.value;
      this.login.token = this.token;
    }
  }
  get f() {
    return this.formGroup.controls;
  }

}
