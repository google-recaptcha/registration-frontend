import { TestBed } from '@angular/core/testing';

import { RecaptchaV2Service } from './recaptcha-v2.service';

describe('RecaptchaV2Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RecaptchaV2Service = TestBed.get(RecaptchaV2Service);
    expect(service).toBeTruthy();
  });
});
