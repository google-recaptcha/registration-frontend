import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Login} from '../../models/login.model';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {GLOBAL} from '../../shared/global';

@Injectable({
  providedIn: 'root'
})
export class RecaptchaV2Service {
  private url: string;
  httpOptions: {};
  constructor(
    private http: HttpClient,
  ) {
    this.url = GLOBAL.url + '/api';
    this.httpOptions = {headers: new HttpHeaders({'Content-Type': 'application/json'})};
  }
  validateEmail(login: Login): Observable<Login> {
    return this.http.post(`${this.url}/validate-email`, login, this.httpOptions).pipe(
      map(response => response as Login)
    );
  }
  validateEmailInvisibleRecaptcha(login: Login): Observable<Login> {
    return this.http.post(`${this.url}/validate-email-invisible-recaptcha`, login, this.httpOptions).pipe(
      map(response => response as Login)
    );
  }
}
