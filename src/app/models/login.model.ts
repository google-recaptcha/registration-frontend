export class Login {
  public email: string;
  public password: string;
  public token: string;
  public status: string;
  constructor() {
    this.email = '';
    this.password = '';
    this.token = '';
    this.status = '';
  }
}
